-- SUMMARY --

Periscope lets you embed profiles anywhere on the web.

The Periscope On Air Button displays the user's live status and username. The special thing about the widget is that it comes to life when you're LIVE!

Clicking the button will open a new window displaying the user's profile page. If the user is LIVE when the profile page loads, their broadcast will automatically start playing.

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Configure your Periscope settings at  Administration » Configuration » Web services » Periscope On Air Button
